require("mocha-allure-reporter");

var assert = require('assert');
var currencyFormat = require('../src/helpers/currencyFormat')

describe('Unit Tests', function()
{
  describe('currencyFormat Positive', function() {
      it('should return -1 when the value is not present', function() {
          allure.createStep(assert.equal(currencyFormat(1), '£1.00'));
      });
  });

  describe('currencyFormat Negative', function() {
      it('should return -1 when the value is not present', function() {
          allure.createStep(assert.equal(currencyFormat(-156), '-£156.00'));
      });
  });


  describe('currencyFormat Decimal Places', function() {
      it('should return -1 when the value is not present', function() {
          allure.createStep(assert.equal(currencyFormat(1.222), '£1.22'));
      });
  });
});

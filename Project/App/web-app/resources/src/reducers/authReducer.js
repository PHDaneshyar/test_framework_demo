import {
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    CLEAR_ALERT
} from '../actions/types'

const initialState = {
    token: localStorage.getItem('token'),
    email: localStorage.getItem('email'),
    isAuthenticated: null,
    isLoading: false,
    user: null,
    loginStatus:0
};

export default function(state = initialState, action){
    switch(action.type){
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            localStorage.setItem('email', action.payload.user.email);
            return {
                ...state,
                ...action.payload,
                isAuthenticated: true,
                isLoading: false,
                email: action.payload.user.email,
                loginStatus:0
            };
        case LOGIN_FAIL:
        case REGISTER_FAIL:
            return{
                ...state,
                token:null,
                user:null,
                isAuthenticated:false,
                isLoading:false,
                email:null,
                loginStatus: action.payload.loginStatus
            };
        case AUTH_ERROR:
        case LOGOUT_SUCCESS:
            localStorage.removeItem('token');
            localStorage.removeItem('email');
            return{
                ...state,
                token:null,
                user:null,
                isAuthenticated:false,
                isLoading:false,
                email:null,
                loginStatus:0
            };
        case CLEAR_ALERT:
            return{
                ...state,
                loginStatus: 0
            }
        default:
            return state;
    }
}
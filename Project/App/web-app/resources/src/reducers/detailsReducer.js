import {
    DETAILS_LOADING,
    DETAILS_LOADED,
    AUTH_ERROR,
    DETAILS_ADDED
} from '../actions/types'

const initialState = {
    isAuthenticated: false,
    isLoading: false,
    userDetails: {
        email: '',
        grade: '',
        currentproject:'',
        pastprojects:[''],
        skills:[''],
        experience:['']
    },
    setup:false
};

export default function(state = initialState, action){
    switch(action.type){
        case DETAILS_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case DETAILS_ADDED:
        case DETAILS_LOADED:
            return {
                ...state,
                isAuthenticated: true,
                isLoading: false,
                userDetails: action.payload.userDetails,
                setup:action.payload.setup
            };
        case AUTH_ERROR:
            return{
                userDetails: {
                    email: '',
                    grade: '',
                    currentproject:'',
                    pastprojects:[''],
                    skills:[''],
                    experience:['']
                },
                isLoading:false,
                isAuthenticated:false
            };
        default:
            return state;
    }
}
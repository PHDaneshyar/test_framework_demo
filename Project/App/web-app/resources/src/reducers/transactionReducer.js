import {
    AUTH_ERROR,
    LOGOUT_SUCCESS,
    PROCESSING_TRANSACTION,
    TRANSACTION_COMPLETE,
    LOADING_TRANSACTIONS,
    TRANSACTIONS_LOADED
} from '../actions/types'

const initialState = {
    isProcessing: false,
    isloading:false,
    transactions:[]
};

export default function(state = initialState, action){
    switch(action.type){
        case PROCESSING_TRANSACTION:
            return {
                ...state,
                isProcessing: true,
            }
        case TRANSACTION_COMPLETE:
            return {
                ...state,
                isProcessing: false,
            }
        case LOADING_TRANSACTIONS:
            return {
                ...state,
                isloading: true,
            }
        case TRANSACTIONS_LOADED:
            return {
                ...state,
                isloading: false,
                transactions: action.payload.transactions
            }
        case AUTH_ERROR:
        case LOGOUT_SUCCESS:
            return{
                ...state,
                isProcessing: false,
            };
        default:
            return state;
    }
}
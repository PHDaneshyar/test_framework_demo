import {combineReducers} from 'redux'
import authReducer from './authReducer'
import errorReducer from './errorReducer'
import detailsReducer from  './detailsReducer'
import projectReducer from './projectReducer'
import userReducer from './userReducer'
import balanceReducer from './balanceReducer'
import transactionReducer from './transactionReducer'

export default combineReducers({
    error: errorReducer,
    auth: authReducer,
    details: detailsReducer,
    project: projectReducer,
    users:userReducer,
    balance:balanceReducer,
    transactions:transactionReducer
});
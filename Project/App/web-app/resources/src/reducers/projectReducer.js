import {
    NEW_PROJECT_ERROR,
    PROJECT_ADDED,
    PROJECTS_LOADING,
    PROJECTS_LOADED,
    AUTH_ERROR
} from '../actions/types';

const initialState = {
    isLoading: false,
    project: {
        name: '',
        startdate: '',
    },
    projects:[]
};

export default function(state = initialState, action){
    switch(action.type){
        case PROJECTS_LOADING:
            return {
                ...state,
                projects:[],
                isLoading:true
            }
        case PROJECTS_LOADED:
            return {
                ...state,
                projects: action.payload.projects,
                isLoading:false,
            };
        case PROJECT_ADDED:
            return {
                ...state,
                project: action.payload.project,
                isLoading:false,
            };
        case AUTH_ERROR:
            return{
                projects: [],
                isLoading:false,
            };
        default:
            return state;
    }
}
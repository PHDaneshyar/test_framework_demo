import {
    AUTH_ERROR,
    USERS_LOADED,
    USERS_LOADING,
    LOGOUT_SUCCESS
} from '../actions/types'

const initialState = {
    isLoading: false,
    users:[]
};

export default function(state = initialState, action){
    switch(action.type){
        case USERS_LOADING:
            return {
                ...state,
                isLoading: true,
                users:[]
            }
        case USERS_LOADED:
            return{
                ...state,
                isLoading: false,
                users: action.payload.users
            }
        case AUTH_ERROR:
        case LOGOUT_SUCCESS:
            return{
                ...state,
                isLoading: false,
                users:[]
            };
        default:
            return state;
    }
}
import {
    AUTH_ERROR,
    LOGOUT_SUCCESS,
    BALANCE_LOADING,
    BALANCE_LOADED,
    BALANCE_UPDATING,
    BALANCE_UPDATED
} from '../actions/types'

const initialState = {
    isLoading: false,
    balance:0
};

export default function(state = initialState, action){
    switch(action.type){
        case BALANCE_UPDATING:
        case BALANCE_LOADING:
            return {
                ...state,
                isLoading: true
            }
        case BALANCE_UPDATED:
        case BALANCE_LOADED:
            return {
                ...state,
                isLoading: false,
                balance: action.payload.balance
            }
        case AUTH_ERROR:
        case LOGOUT_SUCCESS:
            return{
                ...state,
                isLoading: false,
                balance: 0
            };
        default:
            return state;
    }
}
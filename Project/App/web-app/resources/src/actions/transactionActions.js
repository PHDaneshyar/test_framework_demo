import axios from 'axios';

import {
    PROCESSING_TRANSACTION,
    TRANSACTION_COMPLETE,
    LOADING_TRANSACTIONS,
    TRANSACTIONS_LOADED,
    AUTH_ERROR
} from '../actions/types';

// Setup config/headers and token 
export const tokenConfig = getState => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers:{
            "Content-type": "application/json"
        }
    };

    // If token, add to headers
    if(token) {
        config.headers['x_auth_token'] = token
    };

    return config
};

//post email and receive balance

export const sendTransaction = (transaction) => (dispatch, getState)  => {
    dispatch({
        type:PROCESSING_TRANSACTION
    })

    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    transaction.email = email

    axios.post('http://172.23.0.7:4000/transaction/new', transaction, tokenConfig(getState))
        .then(res => dispatch({
            type:TRANSACTION_COMPLETE,
            payload: res.data
        })) 
        .catch(err =>{
            console.log(err)
            dispatch({
                type:AUTH_ERROR
            });
        });
}

export const fetchTransactions = () => (dispatch, getState)  => {
    dispatch({
        type:LOADING_TRANSACTIONS
    })

    var email = getState().auth.email;
    if(!email){
        email = ''
    }


    axios.post('http://172.23.0.7:4000/transaction/fetch', {email:email}, tokenConfig(getState))
        .then(res => dispatch({
            type:TRANSACTIONS_LOADED,
            payload: res.data
        })) 
        .catch(err =>{
            console.log(err)
            dispatch({
                type:AUTH_ERROR
            });
        });
}
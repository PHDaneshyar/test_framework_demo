import axios from 'axios';

import {
    USERS_LOADING,
    USERS_LOADED,
    AUTH_ERROR
} from '../actions/types';

// Setup config/headers and token 
export const tokenConfig = getState => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers:{
            "Content-type": "application/json"
        }
    };

    // If token, add to headers
    if(token) {
        config.headers['x_auth_token'] = token
    };

    return config
};


export const fetchUsers = () => (dispatch, getState) => {
    dispatch({
        type:USERS_LOADING
    })
    axios.get('http://172.23.0.7:4000/users/all', tokenConfig(getState))
        .then(res => dispatch({
            type:USERS_LOADED,
            payload:res.data
        }))
        .catch (err =>{
            dispatch({
                type:AUTH_ERROR
            })
        })
}

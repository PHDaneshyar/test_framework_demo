import axios from 'axios';

import {
    NEW_PROJECT_ERROR,
    PROJECT_ADDED,
    PROJECTS_LOADING,
    PROJECTS_LOADED,
    AUTH_ERROR
} from '../actions/types';

// Setup config/headers and token 
export const tokenConfig = getState => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers:{
            "Content-type": "application/json"
        }
    };

    // If token, add to headers
    if(token) {
        config.headers['x_auth_token'] = token
    };

    return config
};


//Add Project

export const addProject = (newProject) => (dispatch, getState)  => {
    axios.post('http://172.23.0.7:4000/project/add', newProject, tokenConfig(getState))
        .then(res => dispatch({
            type:PROJECT_ADDED,
            payload: res.data
        })) 
        .catch(err =>{
            dispatch({
                type:AUTH_ERROR
            });
        });
}

export const fetchProjects = () => (dispatch, getState) => {
    dispatch({
        type:PROJECTS_LOADING
    })
    axios.get('http://172.23.0.7:4000/project/all', tokenConfig(getState))
        .then(res => dispatch({
            type:PROJECTS_LOADED,
            payload:res.data
        }))
        .catch (err =>{
            dispatch({
                type:AUTH_ERROR
            })
        })
}

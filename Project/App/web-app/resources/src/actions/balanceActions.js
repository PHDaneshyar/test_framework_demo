import axios from 'axios';

import {
    BALANCE_LOADING,
    BALANCE_LOADED,
    BALANCE_UPDATING,
    BALANCE_UPDATED,
    AUTH_ERROR
} from '../actions/types';

// Setup config/headers and token 
export const tokenConfig = getState => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers:{
            "Content-type": "application/json"
        }
    };

    // If token, add to headers
    if(token) {
        config.headers['x_auth_token'] = token
    };

    return config
};

//post email and receive balance

export const fetchBalance = () => (dispatch, getState)  => {
    dispatch({
        type:BALANCE_LOADING
    })

    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    axios.post('http://172.23.0.7:4000/balance', {email:email}, tokenConfig(getState))
        .then(res => dispatch({
            type:BALANCE_LOADED,
            payload: res.data
        })) 
        .catch(err =>{
            console.log(err)
            dispatch({
                type:AUTH_ERROR
            });
        });
}

export const addBalance =  (amount) => (dispatch, getState) => {
    dispatch({
        type:BALANCE_UPDATING
    })

    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    var body = {email:email, amount:amount}

    axios.post('http://172.23.0.7:4000/balance/add', body, tokenConfig(getState))
        .then(res => dispatch({
            type:BALANCE_UPDATED,
            payload: res.data
        })) 
        .catch(err =>{
            console.log(err)
            dispatch({
                type:AUTH_ERROR
            });
        });
}

export const subtractBalance =  (amount) => (dispatch, getState) => {
    dispatch({
        type:BALANCE_UPDATING
    })

    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    var body = {email:email, amount:amount}

    axios.post('http://172.23.0.7:4000/balance/subtract', body, tokenConfig(getState))
        .then(res => dispatch({
            type:BALANCE_UPDATED,
            payload: res.data
        })) 
        .catch(err =>{
            console.log(err)
            dispatch({
                type:AUTH_ERROR
            });
        });
}
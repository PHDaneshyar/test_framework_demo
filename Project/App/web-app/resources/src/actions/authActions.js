import axios from 'axios';
import {returnErrors} from './errorActions'

import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    CLEAR_ALERT,
    CLEAR_ERRORS
} from '../actions/types';


// Setup config/headers and token 
export const tokenConfig = getState => {
        // Get token from localstorage
        const token = getState().auth.token;

        // Headers
        const config = {
            headers:{
                "Content-type": "application/json"
            }
        };
    
        // If token, add to headers
        if(token) {
            config.headers['x_auth_token'] = token
        };

        return config
};

//Register User
export const register = ({email, pass, firstname, lastname}) => dispatch => {
    // Headers
    const config = {
        headers:{
            'Content-Type': 'application/json'
        }
    }

    //Request Body
    const body = JSON.stringify({ email, pass, firstname, lastname})

    axios.post('http://172.23.0.7:4000/users/add', body, config)
        .then(res => {
            if(res.data.loginStatus == 0){
                dispatch({
                    type: REGISTER_SUCCESS,
                    payload: res.data
                })
            } else {
                dispatch({
                    type: REGISTER_FAIL,
                    payload: res.data
                })
            }
        })
        .catch(err => {
        dispatch({
            type : REGISTER_FAIL
        });
    });
};

export const login = ({email, pass}) => dispatch => {
    // Headers
    const config = {
        headers:{
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({email, pass})

    axios.post('http://172.23.0.7:4000/login',body, config)
        .then(res => {
                if(res.data.loginStatus == 0){
                    dispatch({
                        type:LOGIN_SUCCESS,
                        payload: res.data
                    }) 
                } else {
                    dispatch({
                        type:LOGIN_FAIL,
                        payload: res.data
                    }) 
                } 
        })
        .catch(err =>{
            dispatch({
                type:LOGIN_FAIL,
            });
        });   
    }

export const auth = () => (dispatch, getState) => {
    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    const body = JSON.stringify({email})

    axios.post('http://172.23.0.7:4000/auth',body, tokenConfig(getState))
        .then(res => dispatch({
            type:LOGIN_SUCCESS,
            payload: res.data
        }))
        .catch(err =>{
            dispatch(returnErrors(err));
            dispatch({
                type:AUTH_ERROR
            });
        });
};

export const logout = () => (dispatch) => {
    dispatch({
        type:LOGOUT_SUCCESS
    })
}

export const clearAlert = () => (dispatch) => {
    dispatch({
        type:CLEAR_ALERT
    })
}

export const registerError = (errorId) => (dispatch) => {
    dispatch({ 
        type:REGISTER_FAIL,
        payload: {loginStatus:errorId}
    })
}


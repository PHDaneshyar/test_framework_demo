import axios from 'axios';
import {returnErrors} from './errorActions'

import {
    DETAILS_LOADED,
    DETAILS_LOADING,
    AUTH_ERROR,
    DETAILS_ADDED
} from '../actions/types';


export const tokenConfig = getState => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers:{
            "Content-type": "application/json"
        }
    };

    // If token, add to headers
    if(token) {
        config.headers['x_auth_token'] = token
    };

    return config
};

// Check token and load user
export const loadUserDetails = () => (dispatch, getState) => {
    //User Details loading
    dispatch({type:DETAILS_LOADING});

    var email = getState().auth.email;
    if(!email){
        email = ''
    }

    const body = JSON.stringify({email})


    axios.post('http://172.23.0.7:4000/details', body, tokenConfig(getState))
        .then(res => dispatch({
            type:DETAILS_LOADED,
            payload: res.data
        }))
        .catch(err =>{
            dispatch(returnErrors(err));
            dispatch({
                type:AUTH_ERROR
            });
        });
};

export const saveUserDetails = (userDetails) => (dispatch, getState) => {
    axios.post('http://172.23.0.7:4000/details/upsert', userDetails, tokenConfig(getState))
        .then(res => dispatch({
            type:DETAILS_ADDED,
            payload: res.data
        })) 
        .catch(err =>{
            dispatch(returnErrors(err));
            dispatch({
                type:AUTH_ERROR
            });
        });
}

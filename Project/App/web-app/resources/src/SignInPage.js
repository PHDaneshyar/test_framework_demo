import React from 'react';
import './App.css';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom';
import {Provider, connect} from 'react-redux'

import {login, registerError, clearAlert} from './actions/authActions'
import store from'./store'

import LoginAlert from './LoginAlert'
import DashBoard from './DashBoard';



class SignInPage extends React.Component {
    constructor(props){
        super(props);
        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.resetForm = this.resetForm.bind(this)
        this.state = {
            isValidated: false
        }
    }
    static propTypes = {
        isAuthenticated:PropTypes.bool,
        error:PropTypes.object.isRequired,
        login:PropTypes.func.isRequired,
        user:PropTypes.object,
        loginStatus:PropTypes.number,
        registerError:PropTypes.func.isRequired,
        clearAlert:PropTypes.func.isRequired
    }
    componentDidUpdate(prevProps) {
        const { error, isAuthenticated } = this.props;
        if(error !== prevProps.error) {
            // Check for login error
            if(error.id === 'LOGIN_FAIL') {
                this.setState({msg: error.msg})
            } else {
                this.setState({msg: null})
            }
        }

        if(isAuthenticated){
            ReactDOM.render(<Provider store={store}><DashBoard user={this.props.user}></DashBoard></Provider>,document.getElementById('mainbody'))
        }
    }
    SignIn = function(){
        this.props.clearAlert()
        this.setState({isValidated:true})
        if(document.getElementById("loginForm").checkValidity()){
            var email = document.getElementById('loginEmail').value;
            var pass = document.getElementById('loginPassword').value;

            var creds = {
                email:email,
                pass:pass
            }
             this.props.login(creds)
        } else {
            this.props.registerError(2)
        }
    }.bind(this)
    closeAlert = function(){
       this.setState({showAlert:false});
    }.bind(this)
    handleKeyPress(target){
        if(target.charCode == 13){
            this.SignIn()
        }
    }
    resetForm(){
        this.setState({isValidated:false})
    }
  render(){
  return (
    <div className="pagebody">
        <div className="heading">Sign In</div>
        <br></br>
        <div id='alertplaceholder'><LoginAlert alertType={this.props.loginStatus} onClick={this.closeAlert}></LoginAlert></div>
        <br></br>
        <div className="divbox">
            <Form id="loginForm" validated={this.state.isValidated} onFocus={this.resetForm} onKeyPress={this.handleKeyPress} onSubmit = {(e) => this.SignIn()}>
                <Form.Group controlId="loginEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control required type="email" placeholder="Enter email" />
                    <Form.Control.Feedback type="invalid">
                        Please enter a valid email address.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="loginPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control required type="password" placeholder="Password" />
                </Form.Group>
            </Form>
            <div style={{display:'flex',justifyContent:'flex-end'}}><Button className="button-custom" type="button" onClick={(e) => this.SignIn()} variant="primary">Sign In</Button></div>
        </div>
    </div>
  );
  }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error,
    user: state.auth.user,
    loginStatus: state.auth.loginStatus
})

export default connect(mapStateToProps,{login, registerError, clearAlert})(SignInPage);

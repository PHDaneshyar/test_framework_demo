import React from 'react';
import './App.css';
import {connect} from 'react-redux'
import PropTypes from 'prop-types'

import Alert from 'react-bootstrap/Alert'

import {clearAlert} from './actions/authActions'

class RegisterAlert extends React.Component{
    static propTypes = {
      loginStatus:PropTypes.number,
      clearAlert:PropTypes.func
    }
    switchHeading(param){
      switch(param){
        case 4:
          return 'Passwords Do Not Match'
        case 5:
          return 'Register Error'
        case 6:
          return 'User Already Exists'
        case 7:
          return 'Register Error'

      }
    }
    switchMessage(param){
        switch(param){
          case 4:
            return 'Make sure both password and confirmation match.'
          case 5:
            return 'Please check below and try again.'
          case 6:
            return 'Try Logging in with the same email.'
          case 7:
            return 'Please try again.'
        }
      }
    closeAlert(){
      this.props.clearAlert()
    }
    render(){
    if (this.props.loginStatus != 0 && this.props.loginStatus > 3) {
      return (
        <Alert id='alertlogin' onClose={(e) => this.closeAlert(e)} className="alertfadein" variant="danger" dismissible>
        <Alert.Heading>{this.switchHeading(this.props.loginStatus)}</Alert.Heading>
        <p>{this.switchMessage(this.props.loginStatus)}</p>
    </Alert>
      );
    }
    return '';
  }
}

const mapStateToProps = state => ({
  loginStatus: state.auth.loginStatus
})

export default connect(mapStateToProps, {clearAlert})(RegisterAlert);

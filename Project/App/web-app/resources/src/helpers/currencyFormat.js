function currencyFormat(num) {
    var formatter = Intl.NumberFormat('en-GB', {style:'currency',currency:'GBP',minimumFractionDigits:2})
    return formatter.format(num)
  }

module.exports = currencyFormat
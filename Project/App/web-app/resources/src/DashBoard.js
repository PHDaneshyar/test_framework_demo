import React from 'react';
import './App.css';
import {connect, Provider} from 'react-redux'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom';
import store from './store'
import {GoogleCharts} from 'google-charts';

import {fetchBalance} from './actions/balanceActions'
import {fetchTransactions} from './actions/transactionActions'
import currencyFormat from './helpers/currencyFormat'

import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'

import DepositPage from './DepositPage';
import WithdrawalPage from './WithdrawalPage';


class DashBoard extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            cleanTransactions : null,
            transactionsCleaned : false,
            reversedTransactions : []
        }
        this.drawBalanceChart = this.drawBalanceChart.bind(this)
    }
    static propTypes = {
        isLoading:PropTypes.bool,
        balance:PropTypes.number,
        fetchBalance:PropTypes.func.isRequired,
        fetchTransactions:PropTypes.func.isRequired,
        transactionsLoading:PropTypes.bool,
        transactions: PropTypes.array

    }
    componentDidUpdate(prevProps){
        if(this.props !== prevProps){
            //this.setState({cleanTransactions:this.cleanTransactions(this.props.transactions), transactionsCleaned:true})
            if(!this.props.transactionsLoading){
                this.TransactionHTML(this.cleanTransactions(this.props.transactions))
                this.drawBalanceChart()
            }
        }       
    }
    componentDidMount(){
        this.props.fetchBalance()
        this.props.fetchTransactions()
        window.onresize = this.drawBalanceChart
    }
    drawBalanceChart(){
        var reversedTransactions = this.props.transactions.slice().reverse()
        GoogleCharts.load(drawChart);

        var plottxns = [['Time', 'Balance']]

        var running_balance = 0

        reversedTransactions.map(transaction => {
            running_balance += transaction.amount/100
            plottxns.push([new Date(transaction.date), running_balance])
        })
 
        function drawChart() {
            var data = GoogleCharts.api.visualization.arrayToDataTable(plottxns);
    
            var options = {
              legend: {position: 'none'},
              hAxis: {
                format: 'd/M/yy',
                gridlines: {color: 'none'}
              },
              vAxis: {
                  gridlines: {count: 0}
              },
              series: {
                0: { color: '#629460' },
              },
              lineWidth: 5
            };
    
            var chart = new GoogleCharts.api.visualization.LineChart(document.getElementById('balancechart'));
    
            chart.draw(data, options);
        }
    }
    cleanTransactions(arrTransactions){
        var arrOut = []
        var arrInner = []

        var dates = [... new Set(Object.values(arrTransactions).map(value => new Date(value.date).getDate() +'/' +  parseInt(new Date(value.date).getMonth() + 1) + '/' + new Date(value.date).getFullYear()))]

        for(var date of dates){
            arrInner = []
            for(var transaction of arrTransactions){
                if((new Date(transaction.date).getDate() +'/' +  parseInt(new Date(transaction.date).getMonth() + 1) + '/' + new Date(transaction.date).getFullYear()) == date){
                    arrInner.push(transaction)
                }
            }
            arrOut.push(arrInner)
        }

        return arrOut

    }
    TransactionHTML(transactions){
        var elementsOut = []
        transactions.map(innerTransactions => {
           elementsOut.push(<div>
                                <Row>
                                    <Col className="coldate">{new Date(innerTransactions[0].date).getDate() +'/' +  parseInt(new Date(innerTransactions[0].date).getMonth() + 1) + '/' + new Date(innerTransactions[0].date).getFullYear()}</Col>
                                </Row>
                                <div id={innerTransactions[0].date}>
                                </div>
                            </div>)
            innerTransactions.map(transaction => {
           elementsOut.push(
                <Row className="rowtxn">
                <Col>{transaction.desc}</Col>
                <Col className="colamount">{currencyFormat(parseInt(transaction.amount)/100)}</Col>
            </Row>)
            })
        })
        ReactDOM.render(elementsOut, document.getElementById('transactionsplaceholder'))

    }
    gotoDepositPage(){
        ReactDOM.render(<Provider store={store}><DepositPage></DepositPage></Provider>, document.getElementById('mainbody'))
    }
    gotoWithdrawalPage(){
        ReactDOM.render(<Provider store={store}><WithdrawalPage></WithdrawalPage></Provider>, document.getElementById('mainbody'))
    }
    render(){
        return(
            <div>
                <div id="dashboard" className="pagebody">
                    <div className="dashboardheading">Account Number: ####</div>
                    <br></br>
                    {this.props.isLoading &&
                        <div className="dashboardbalance">...</div>
                    }
                    {!this.props.isLoading &&
                        <div className="dashboardbalance">{currencyFormat(parseInt(this.props.balance)/100)}</div>
                    }
                    <br></br>
                    <div className="graphbox">
                        <div id='balancechart'></div>
                    </div>
                    <div className="spacer"></div>
                    <div id="transactionsplaceholder" className="transactions"></div>
                    <div className="spacer"></div>
                    <div style={{display:'flex',justifyContent:'flex-end'}}>
                        <Button onClick={this.gotoDepositPage} style={{marginRight:'1%'}} className="button-custom" type="button" variant="primary">Deposit</Button>
                        <Button onClick={this.gotoWithdrawalPage} className="button-custom" type="button" variant="primary">Withdraw</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    balance: state.balance.balance,
    isLoading : state.balance.isLoading,
    transactionsLoading : state.transactions.isLoading,
    transactions: state.transactions.transactions
})


export default connect(mapStateToProps, {fetchBalance, fetchTransactions})(DashBoard);

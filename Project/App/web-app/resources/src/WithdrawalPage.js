import React from 'react';
import './App.css';
import {connect, Provider} from 'react-redux'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'

import DashBoard from './DashBoard'

import {sendTransaction} from './actions/transactionActions'
import {subtractBalance} from './actions/balanceActions'

import store from './store'

import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import TransactionModal from './TransactionModal';

class WithdrawalPage extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            isValidated: false,
            showModal: false
        }
        this.withdraw = this.withdraw.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }
    static propTypes = {
        isProcessing:PropTypes.bool,
        sendTransaction:PropTypes.func.isRequired,
        subtractBalance:PropTypes.func.isRequired
    }
    withdraw(){
        this.setState({isValidated:true})
        if(document.getElementById("withdrawalform").checkValidity()){
            var currDate = new Date()

            var newTransaction = {
                amount: parseInt(document.getElementById('txn-amount').value * -100),
                desc: document.getElementById('txn-desc').value,
                date: currDate.getTime()
            }

            this.setState({showModal:true})

            this.props.sendTransaction(newTransaction)
            this.props.subtractBalance(newTransaction.amount * -1)

            setTimeout(this.closeModal, 1000)
        }
    }
    closeModal(){
        this.setState({showModal:false})
        ReactDOM.render(<Provider store={store}><DashBoard user={this.props.user}></DashBoard></Provider>, document.getElementById('mainbody'))
    }
    render(){
        return(
            <div className="pagebody">
                <TransactionModal showModal={this.state.showModal}></TransactionModal>
                <div className="dashboardheading">Withdrawal</div>
                <div className="depositform">
                    <Form id="withdrawalform" validated={this.state.isValidated}>
                        <Form.Group controlId="txn-amount">
                            <Form.Label>Amount</Form.Label>
                            <Form.Control required type="number" placeholder="" />
                        </Form.Group>
                        <Form.Group controlId="txn-desc">
                            <Form.Label>Description</Form.Label>
                            <Form.Control required type="text" placeholder="" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Bank Account Number</Form.Label>
                            <Form.Control required type="number"></Form.Control>
                        </Form.Group>
                        <Form.Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control required type="text"></Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control required type="text"></Form.Control>
                                </Form.Group>
                            </Col>
                        </Form.Row>
                    </Form>
                    <div style={{display:'flex',justifyContent:'flex-end'}}><Button onClick={this.withdraw} className="button-custom" type="button" variant="primary">Withdraw</Button></div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({ 
    isProcessing : state.transactions.isProcessing,
})


export default connect(mapStateToProps, {sendTransaction, subtractBalance})(WithdrawalPage);

import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal'

class TransactionModal extends Component{
  constructor(props){
      super(props)
  }
  render(){
    return (
        <Modal dialogClassName="transactionsmodal" centered show={this.props.showModal}>
            <Modal.Body>
                <div className="dashboardheading">Processing Transaction</div>
                <br></br>
                <div style={{textAlign:"center"}}>
                    <div className="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <br></br>
            </Modal.Body>
        </Modal>
    );
  }
}

export default TransactionModal;

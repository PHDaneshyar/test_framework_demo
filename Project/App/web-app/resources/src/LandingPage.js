import React, { Component } from 'react';
import './App.css';
import Navbar from './Navbar';

import {Provider} from 'react-redux';
import store from './store';


class LandingPage extends Component{
  render(){
    return (
      <Provider store={store}>
        <div id="maincontent">
            <Navbar></Navbar>
            <div id="mainbody">
            </div>
        </div>
      </Provider>
    );
  }
}

export default LandingPage;

import React from 'react';
import './App.css';
import _Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import ReactDOM from "react-dom";
import {Provider, connect} from 'react-redux'
import PropTypes from 'prop-types'

import store from './store'

import {auth, logout, clearAlert} from './actions/authActions'

import SignInPage from './SignInPage.js'
import RegisterPage from './RegisterPage.js'
import DashBoard from './DashBoard'

class Navbar extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      isLoggedIn:false
    }
  }
  static propTypes = {
      isAuthenticated:PropTypes.bool,
      error:PropTypes.object.isRequired,
      user:PropTypes.object,
      auth:PropTypes.func.isRequired,
      logout:PropTypes.func.isRequired,
      clearAlert:PropTypes.func.isRequired
  }
  componentDidMount(){
    this.props.auth()
  }
  loadSignInPage(){
    this.props.clearAlert()
    ReactDOM.render(<Provider store={store}><SignInPage></SignInPage></Provider>,document.getElementById('mainbody'))
  }
  loadRegisterPage(){
    this.props.clearAlert()
    ReactDOM.render(<Provider store={store}><RegisterPage></RegisterPage></Provider>,document.getElementById('mainbody'))
  }
  loadDashBoard(){
    ReactDOM.render(<Provider store={store}><DashBoard></DashBoard></Provider>,document.getElementById('mainbody'))
  }
  logout(){
    this.props.logout()
    ReactDOM.render(<Provider store={store}><SignInPage></SignInPage></Provider>,document.getElementById('mainbody'))
  }
  render(){
    if(!this.props.isAuthenticated){
      return (
        <_Navbar bg="custom" className="navbar-custom">
          <_Navbar.Brand className="navbar-custom"><span style={{fontStyle:"italic"}}>DMX</span><span> Platform</span></_Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link className="navbar-custom" onClick={(e) => this.loadSignInPage(e)}>Sign In</Nav.Link>
            <Nav.Link className="navbar-custom" onClick={(e) => this.loadRegisterPage(e)}>Register</Nav.Link>
          </Nav>
        </_Navbar>
      );
    } else {
      return (
        <_Navbar className="navbar-custom">
          <_Navbar.Brand className="navbar-light navbar-custom"><span style={{fontStyle:"italic"}}>DMX</span><span> Platform</span></_Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link className="navbar-custom" onClick={(e) => this.loadDashBoard(e)}>Home</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link className="navbar-custom" onClick={(e) => this.logout(e)} className="justify-content-end">Log Out</Nav.Link>
          </Nav>
        </_Navbar>
      );
    }

  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
})

export default connect(mapStateToProps,{auth, logout, clearAlert})(Navbar);


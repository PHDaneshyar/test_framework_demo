import React from 'react';
import './App.css';

import {connect} from 'react-redux'
import PropTypes from 'prop-types'

import {clearAlert} from './actions/authActions'

import Alert from 'react-bootstrap/Alert'


class LoginAlert extends React.Component{
  static propTypes = {
    loginStatus:PropTypes.number,
    clearAlert:PropTypes.func
  }
  switchHeading(param){
    switch(param){
      case 1:
        return 'Incorrect Password'
      case 2:
        return 'Login error'
      case 3:
        return 'User does not exist'        
      default:
        return ''
    }
  }
  closeAlert(){
    this.props.clearAlert()
  }
  render(){
    if (this.props.loginStatus !== 0 && this.props.loginStatus < 4) {
      return (
        <Alert onClose={(e) => this.closeAlert(e)} id='alertlogin' className="alertfadein" variant="danger" dismissible>
        <Alert.Heading>{this.switchHeading(this.props.loginStatus)}</Alert.Heading>
        <p>Please try again.</p>
    </Alert>
      );
    }
    return '';
  }
}

const mapStateToProps = state => ({
  loginStatus: state.auth.loginStatus
})


export default connect(mapStateToProps,{clearAlert})(LoginAlert);

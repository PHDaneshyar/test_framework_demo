
class dbObject{
    constructor(filepath){
        this.filepath = filepath
    }
    loadData(){
        const fs = require('fs');
        var data = fs.readFileSync(this.filepath, {encoding: 'utf8'})
        return JSON.parse(data)       
    }
    findAll(){
        var json = this.loadData()
        var arrOut = []
        for(var key of Object.keys(json)){
            arrOut.push(json[key])
        }
        return arrOut
    }
    query(objQuery){
        var arrIn = this.findAll()
        var arrOut = []
        for(var record of arrIn){
            if(record[Object.keys(objQuery)[0]] == Object.values(objQuery)[0] && record[Object.keys(objQuery)[1]] == Object.values(objQuery)[1]){
                arrOut.push(record)
            }
        }
        return arrOut
    }
    insert(arrIn){
        var currArr = this.findAll()
        var updArr = currArr.concat(arrIn)
        var ObjAdd = {}
        for(var item of updArr){
            ObjAdd[item.id] = item
        }
        const fs = require('fs');
        fs.writeFileSync(this.filepath, JSON.stringify(ObjAdd), {encoding: 'utf8'})
    }
    update(objIn, id){
        var json = this.loadData()
        for(var i in Object.keys(objIn)){
            json[id][Object.keys(objIn)[i]] = Object.values(objIn)[i]
        }
        const fs = require('fs');
        fs.writeFileSync(this.filepath, JSON.stringify(json), {encoding: 'utf8'})
    }
    upsert(objIn, id){
        var keys = Object.keys(this.loadData())
        if(keys.includes(id)){
            this.update(objIn, id)
        }
        else{
            this.insert([objIn])
        }
    }
    getRecord(id){
        var json = this.loadData()
        return json[id]
    }
    like(objQuery){
        var arrIn = this.findAll()
        var arrOut = []
        var flag 
        for(var record of arrIn){
            flag = true
            var i = 0
            while(i < Object.keys(objQuery).length && flag == true){
                if(!record[Object.keys(objQuery)[i]].includes(Object.values(objQuery)[i])){
                    flag = false
                }
                i++
            }
            if(flag === true){
                arrOut.push(record)
            }
        }
        return arrOut
    }
}

export default dbObject
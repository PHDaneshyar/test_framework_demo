import React from 'react';
import ReactDOM from 'react-dom'
import './App.css';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import {Provider, connect} from 'react-redux'
import PropTypes from 'prop-types'
import store from './store'

import RegisterAlert from './RegisterAlert'
import DashBoard from './DashBoard'

import { register, registerError, clearAlert } from './actions/authActions'
import { clearErrors } from './actions/errorActions'

class RegisterPage extends React.Component{
    constructor(props){
        super(props)
        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.resetForm = this.resetForm.bind(this)
        this.state = {
            isValidated: false
        }
    }
    state = {
        showAlert:false,
        alertType:null,
        email:'',
        pass:'',
        firstname:'',
        lastname:''
    }
    
    static propTypes = {
        isAuthenticated:PropTypes.bool,
        error:PropTypes.object.isRequired,
        register:PropTypes.func.isRequired,
        clearAlert:PropTypes.func.isRequired,
        registerError:PropTypes.func.isRequired,
        user:PropTypes.object,
        loginStatus:PropTypes.number
    }

    componentDidUpdate(prevProps) {
        const { error, isAuthenticated } = this.props;
        if(isAuthenticated){
            ReactDOM.render(<Provider store={store}><DashBoard user={this.props.user}></DashBoard></Provider>,document.getElementById('mainbody'))
        }
    }

    Register = function(){
        this.setState({isValidated:true})
        this.props.clearAlert();
        if(document.getElementById("registerForm").checkValidity()){
            if(document.getElementById("loginPassword").value == document.getElementById("loginPasswordConfirm").value){
                var email = document.getElementById("loginEmail").value
                var pass = document.getElementById("loginPassword").value
                var firstname = document.getElementById("loginFirstName").value
                var lastname = document.getElementById("LoginLastName").value
    
                var newUser = {
                    email:email,
                    pass:pass,
                    firstname:firstname,
                    lastname:lastname
                }
    
                this.props.register(newUser)
            } else{
                this.props.registerError(4)
            }
        } else {
            this.props.registerError(5)           
        }
    }.bind(this)
    handleKeyPress(target){
        if(target.charCode == 13){
            this.Register()
        }
    }
    resetForm(){
        this.setState({isValidated:false})
    }
    render(){
        return (
            <div className="pagebody">
                <div className="heading">Register</div>
                <br></br>
                <div id='alertplaceholder'><RegisterAlert></RegisterAlert></div>
                <div className="divbox">
                    <Form id='registerForm' onFocus={this.resetForm} validated={this.state.isValidated} onKeyPress={this.handleKeyPress} onSubmit = {(e) => this.Register()}>
                        <Form.Group controlId="loginEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control required type="email" placeholder="Enter email" />
                            <Form.Control.Feedback type="invalid">
                            Please enter a valid email address.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="loginPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control required type="password" placeholder="Password" />
                        </Form.Group>
                        <Form.Group controlId="loginPasswordConfirm">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control required type="password" placeholder="Confirm Password" />
                        </Form.Group>
                        <Form.Group controlId="loginFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control required type="text" placeholder="First Name" />
                        </Form.Group>
                        <Form.Group controlId="LoginLastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control required type="text" placeholder="Last Name" />
                        </Form.Group>
                    </Form>
                    <div style={{display:'flex',justifyContent:'flex-end'}}><Button className="button-custom" onClick={this.Register} variant="primary">Register</Button></div>
                </div>
            </div>
        );
        }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error,
    user: state.auth.user,
    loginStatus: state.auth.loginStatus
})

export default  connect(mapStateToProps,{register, clearAlert, registerError})(RegisterPage);

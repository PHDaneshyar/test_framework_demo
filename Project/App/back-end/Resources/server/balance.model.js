const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Balance = new Schema({
    email:{
        type:String
    },
    balance:{
        type:Number
    }
})

module.exports = mongoose.model('balance', Balance);
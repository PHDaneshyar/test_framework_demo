const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Project = new Schema({
    name:{
        type:String
    },
    startdate:{
        type:Number
    }
})

module.exports = mongoose.model('project', Project);
const express = require('express');
const app = express();
// const appBalancer = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')
const config  = require('config');
const jwt = require('jsonwebtoken');
const auth = require('./middleware/auth')
// var request = require('request');

const PORT = 4000;

let User = require('./user.model');
let UserDetails = require('./userdetails.model')
let Project = require('./project.model')
let Transaction = require('./transaction.model')

const userRoutes = express.Router();
const loginRoutes = express.Router();
const userDetailsRoutes = express.Router();
const authRoutes = express.Router();
const projectRoutes = express.Router();
const balanceRoutes = require('./routes/balanceRoutes')
const transactionRoutes = require('./routes/transactionRoutes')

app.use(cors())
app.use(bodyParser.json())

console.log(process.env)


var mongoUrl = "mongodb://localhost:27017/test"

var connectWithRetry = function() {
  return mongoose.connect(config.get('mongoURI'),{useNewUrlParser: true}, function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 15 sec', err);
      setTimeout(connectWithRetry, 15000);
    }
  });
};
connectWithRetry();


// mongoose.connect('mongodb://radsam:88318831@172.23.0.8:27017/docker-app',{useNewUrlParser: true})
const connection = mongoose.connection;

loginRoutes.route('/').post(function(req, res) {
    let creds = req.body;
    User.findOne({email:creds.email}, function(err, user){
        if(err){
            res.json({loginStatus:2})
        } else if (!user){
            res.json({loginStatus:3})
        } else {
            if(user.pass == creds.pass){
                jwt.sign(
                    {email:user.email},
                    config.get('jwtSecret'),
                    {expiresIn: 300},
                    (err, token) => {
                        if(err){
                            res.status(200).json({loginStatus:2,user:user,token:token});
                        } else {
                            res.status(200).json({loginStatus:0,user:user,token:token});
                        }
                    }
                ) //Generating auth token
            } else {
                res.json({loginStatus:1})
            }
        }
    }.bind(creds));
});

userRoutes.route('/all').get(auth, function(req, res) {
    User.find(function(err, users) {
        if(err) {
            console.log(err);
        } else {
            res.json(users);
        }
    });
});

userRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    User.findById(id, function(err, user){
        if(err) {
            console.log(err)
        } else {
            res.json(user)
        }
    })
})

userRoutes.route('/add').post(function(req, res) {
    User.findOne({email:req.body.email}, function(err, usercheck){
        if(usercheck){
            res.json({loginStatus:6});
        } else {
            let user = new User(req.body);
            user.save().then(user => {
                user.pass = null
                jwt.sign(
                    {email:user.email},
                    config.get('jwtSecret'),
                    {expiresIn: 300},
                    (err, token) => {
                        if(err) throw err;
                        res.status(200).json({loginStatus:0,user:user,token:token});
                    }
                ) //Generating auth token
            })
            .catch(err =>{
                res.json({loginStatus:7});
            });
        }
    })

});


userRoutes.route('/update/:id').post(function(req, res) {
    let id = req.params.id;
    User.findById(id, function(err, user) {
        if (!user){
            res.status(404).send('data is not found');
        } else {
            user.email = req.body.email;
            user.pass = req.body.pass;
            user.firstname = req.body.firstname;
            user.lastname = req.body.lastname;

            user.save().then(todo => {
                res.json('User updated');
            })
            .catch(err => {
                res.status(400).send("Update not possible")
            });
        }
    });
});

userDetailsRoutes.route('/',).post(auth, function(req, res) {
    let email = req.body.email
    UserDetails.findOne({email:email}, function(err, userDetails){
        if(err){
        } else if (!userDetails) {
            res.json({setup:true,userDetails:{email:"",grade:"", currentproject:"", pastprojects: [''], skills: [''], experience: ['']}})
        } else {
            res.json({setup:false, userDetails:userDetails})
        }
    })
})

userDetailsRoutes.route('/upsert').post(auth, function(req, res) {

    let userDetailsIn = new UserDetails(req.body);
    UserDetails.findOne({email:userDetailsIn.email}, function(err,userDetails){
        if(userDetails){
            userDetails.email = userDetailsIn.email
            userDetails.grade = userDetailsIn.grade
            userDetails.currentproject = userDetailsIn.currentproject
            userDetails.pastprojects = userDetailsIn.pastprojects
            userDetails.skills = userDetailsIn.skills
            userDetails.experience = userDetailsIn.experience

            userDetails.save().then(userDetails => {
                res.status(200).json({setup:false, userDetails:userDetails});
            })
            .catch(err =>{
                res.status(400).json({setup:false, msg:'updating user details failed', userDetails:{email:"",grade:"", currentproject:"", pastprojects: [''], skills: [''], experience: ['']}})
            });
        } else {
            userDetailsIn.save().then(userDetails => {
                res.status(200).json({setup:false, msg: 'user details added successfully',userDetails:userDetails});
            })
            .catch(err =>{
                res.status(400).json({setup:true, msg:'adding user details failed', userDetails:{email:"",grade:"", currentproject:"", pastprojects: [''], skills: [''], experience: ['']}})
            });
        }
    })
})

userDetailsRoutes.route('/update').post(function(req, res) {
    let userDetails = new UserDetails(req.body);
    UserDetails.findOneAndUpdate({email:userDetails.email},userDetails).then(userDetails => {
        res.status(200).json({msg: 'user details updated successfully',userDetails:userDetails});
    })
    .catch(err =>{
        res.status(400).json({msg:'updating user failed'})
    });
})

authRoutes.route('/').post(auth, (req, res)=>{
    let email = req.body.email;
    User.findOne({email:email})
        .select('-pass')
        .then(user => {
            jwt.sign(
                {email:user.email},
                config.get('jwtSecret'),
                {expiresIn: 300},
                (err, token) => {
                    res.status(200).json({msg: 'auth successful',user:user,token:token});
                })
    }).catch(err =>{
        res.status(401).json({msg:'Not Authenticated',err:err})
    })
})

projectRoutes.route('/add').post(auth, (req, res)=>{
    Project.findOne({name:req.body.name}, function(err, projectcheck) {
        if(projectcheck){
            res.status(401).json({msg:'project exists'})
        } else {
            let project = new Project(req.body)
            project.save().then(project => {
                res.status(200).json({msg:'project added', project:project})
            }).catch(err =>{
                res.status(400).json({msg:'saving error', err:err})
            })
        }
    })
})

projectRoutes.route('/all').get(auth, (req, res)=>{
    Project.find(function(err, projects) {
        if(projects){
            res.status(200).json({msg:'fetched projects', projects:projects})
        } else {
            res.status(400).json({msg:'error loading projects'})
        }
    }).catch(err =>{
        res.status(401).json({msg:'Not Authenticated', err:err})
    })
})


app.use('/login', loginRoutes);
app.use('/users', userRoutes);
app.use('/details', userDetailsRoutes);
app.use('/auth', authRoutes)
app.use('/project', projectRoutes)
app.use('/balance', balanceRoutes)
app.use('/transaction', transactionRoutes)

app.listen(PORT, function(){
    console.log("Server is running on Port: " + PORT);
});


// appBalancer.get('/prod', function(req,res) {
//     //modify the url in any way you want
//     var newurl = 'http://google.com/';
//     request(newurl).pipe(res);
//   });

//   appBalancer.get('/', function(req,res) {
//     //modify the url in any way you want
//     var newurl = 'http://172.23.0.5:3000';
//     request(newurl).pipe(res);
//   });

// appBalancer.listen(4001)


connection.once('open', function(){
    console.log("MongoDB database connection established successfully")
})
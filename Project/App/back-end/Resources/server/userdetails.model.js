const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserDetails = new Schema({
    email:{
        type:String
    },
    grade:{
        type:String
    },
    currentproject:{
        type:String
    },
    pastprojects:{
        type:[String]
    },
    skills:{
        type:[String]
    },
    experience:{
        type:[String]
    }
})

module.exports = mongoose.model('userdetails', UserDetails);
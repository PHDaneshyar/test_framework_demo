const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
    email:{
        type:String
    },
    pass:{
        type:String
    },
    firstname:{
        type:String
    },
    lastname:{
        type:String
    }
})

module.exports = mongoose.model('user', User);
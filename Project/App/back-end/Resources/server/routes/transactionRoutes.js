express = require('express')

const auth = require('../middleware/auth')

router = express.Router()

let Transaction = require('../transaction.model')

router.route('/fetch').post(auth, function(req, res) {
    let email = req.body.email;
    console.log('txns')
    Transaction.find({email:email}).sort({date: -1}).exec(function(err, transactions) {
        if(err) {
            console.log(err);
        } else if(!transactions){
            res.json({transactions:[]})
        } else {
            res.json({transactions:transactions});
        }
    });
})

router.route('/new').post(auth, function(req, res) {
    let newTransaction = new Transaction(req.body)
    newTransaction.save().then(transaction => {
        res.status(200).json({msg:'transaction complete', transaction:transaction})
    }).catch(err =>{
        res.status(400).json({msg:'transaction error', err:err})
    });
})


module.exports = router;
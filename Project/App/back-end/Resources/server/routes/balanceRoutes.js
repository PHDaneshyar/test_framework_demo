express = require('express')

const auth = require('../middleware/auth')

router = express.Router()

let Balance = require('../balance.model')


//Recieve email address and send balance
router.route('/').post(auth, function(req, res) {
    let email = req.body.email;
    Balance.findOne({email:email}, function(err, balance) {
        if(!balance) {
            res.json({balance:0});
        } else {
            res.json(balance);
        }
    });
});

//add to balance
router.route('/add').post(auth, function(req, res){
    let email = req.body.email
    let amount = req.body.amount
    Balance.findOne({email:email}, function(err, balance) {
        if(!balance){
            balance = new Balance({email:email, balance:amount})
            balance.save()
        } else {
            balance.balance = parseInt(amount) + parseInt(balance.balance)
            balance.save()
            res.json(balance);
        }
    })
})

//subtract from balance
router.route('/subtract').post(auth, function(req, res){
    let email = req.body.email
    let amount = req.body.amount
    Balance.findOne({email:email}, function(err, balance) {
        if(!balance){
            balance = new Balance({email:email, balance:amount})
            balance.save()
        } else {
            balance.balance = parseInt(balance.balance) - parseInt(amount)
            balance.save()
            res.json(balance);
        }
    })
})


module.exports = router;
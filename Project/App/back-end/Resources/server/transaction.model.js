const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Transaction = new Schema({
    email:{
        type:String
    },
    date:{
        type:Number
    },
    amount:{
        type:Number
    },
    desc:{
        type:String
    }
})

module.exports = mongoose.model('transaction', Transaction);
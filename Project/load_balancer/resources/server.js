const express = require('express');
const request = require('request');
var ping = require('ping');

const servers = ['http://172.23.0.23:3000', 'http://172.23.0.5:3000'];
let cur = 0;
var prevtime = new Date().getTime()
var currtime = 0



const handler = (req, res) => {
  // Pipe the vanilla node HTTP request (a readable stream) into `request`
  // to the next server URL. Then, since `res` implements the writable stream
  // interface, you can just `pipe()` into `res`.
  currtime = new Date().getTime()
  next_server = servers[(cur + 1) % (servers.length)].split('//')[1].split(':')[0]
  ping.promise.probe(next_server)
  .then(function (res) {
    if(res.alive == true){
      if(currtime - prevtime > 500){
        cur = (cur + 1) % (servers.length);
      }
        console.log('Sending request to next host: ' + servers[cur])
      } else {
        console.log('other address is down staying with current host')
    }
    prevtime = currtime
  });
  req.pipe(request({ url: servers[cur] + req.url })).pipe(res);
};
const server = express().get('*', handler).post('*', handler);

server.listen(80);

//var newurl = 'http://172.23.0.5:3000';
//     request(newurl).pipe(res);
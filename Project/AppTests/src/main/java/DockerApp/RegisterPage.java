package DockerApp;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class RegisterPage
{
    private RemoteWebDriver driver;

    public RegisterPage(RemoteWebDriver driver)
    {
        this.driver = driver;
    }

    @Step("navigate to registration page")
    public void GoToRegisterPage()
    {
        driver.findElement(By.xpath("//a[@class='navbar-custom nav-link' and contains(text(), 'Register')]")).click();
    }

    @Step("creating an account: email: {0}, password: {1}, password2: {2}, first name: {3}, last name: {4}")
    public void EnterDetails(String email, String password, String password2, String firstName, String lastName)
    {
        driver.findElement(By.id("loginEmail")).sendKeys(email);
        driver.findElement(By.id("loginPassword")).sendKeys(password);
        driver.findElement(By.id("loginPasswordConfirm")).sendKeys(password2);
        driver.findElement(By.id("loginFirstName")).sendKeys(firstName);
        driver.findElement(By.id("LoginLastName")).sendKeys(lastName);
    }

    public String PasswordErrorMessage()
    {
        return driver.findElement(By.xpath("//div[@class='alert-heading h4']")).getText();
    }

    public String EmailErrorMessage()
    {
        return driver.findElement(By.className("invalid-feedback")).getText();
    }

    public void Register()
    {
        driver.findElement(By.xpath("//button[contains(text(), 'Register')]")).click();
    }
}

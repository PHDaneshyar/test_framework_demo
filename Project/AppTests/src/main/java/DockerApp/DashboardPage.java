package DockerApp;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

public class DashboardPage
{
    private RemoteWebDriver driver;

    public DashboardPage(RemoteWebDriver driver)
    {
        this.driver = driver;
    }

    @Step("Making withdrawal, Amount: {0} AccountNum: {2} First Name: {3} Last Name: {4}")
    public void Withdraw(int amount, String description, int accNum, String firstName, String lastName)
    {
        driver.findElement(By.xpath("//button[contains(text(), 'Withdraw')]")).click();
        driver.findElement(By.id("txn-amount")).sendKeys(Integer.toString(amount));
        driver.findElement(By.id("txn-desc")).sendKeys(description);
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[3]/input")).sendKeys(Integer.toString(accNum));
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[4]/div[1]/div/input")).sendKeys(firstName);
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[4]/div[2]/div/input")).sendKeys(lastName);
        driver.findElement(By.xpath("//button[contains(text(), 'Withdraw')]")).click();
    }

    @Step("Checking most recent transaction")
    public String TransactionCheck()
    {
        String txn = driver.findElement(By.xpath("//*[@id='transactionsplaceholder']/div[2]/div[2]")).getText();
        return txn;
    }

    @Step("Making Deposit, Amount: {0} AccountNum: {2} First Name: {3} Last Name: {4}")
    public void Deposit(int amount, String description, int accNum, String firstName, String lastName)
    {
        driver.findElement(By.xpath("//button[contains(text(), 'Deposit')]")).click();
        driver.findElement(By.id("txn-amount")).sendKeys(Integer.toString(amount));
        driver.findElement(By.id("txn-desc")).sendKeys(description);
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[3]/input")).sendKeys(Integer.toString(accNum));
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[4]/div[1]/div/input")).sendKeys(firstName);
        driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/form/div[4]/div[2]/div/input")).sendKeys(lastName);
        driver.findElement(By.xpath("//button[contains(text(), 'Deposit')]")).click();
    }
}

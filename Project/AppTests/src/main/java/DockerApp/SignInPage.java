package DockerApp;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SignInPage
{
    private RemoteWebDriver driver;

    public SignInPage(RemoteWebDriver driver)
    {
        this.driver = driver;
    }

    @Step("Go to Sign In tab")
    public void SignInTab()
    {
        driver.findElement(By.xpath("//a[@class='navbar-custom nav-link' and contains(text(), 'Sign In')]")).click();
    }

    @Step("Enter email: {0}")
    public void Email(String email)
    {
        driver.findElement(By.id("loginEmail")).sendKeys(email);
    }

    @Step("Enter Password {0)")
    public void Password(String password)
    {
        driver.findElement(By.id("loginPassword")).sendKeys(password);
    }

    public String ErrorMessage()
    {
        return driver.findElement(By.xpath("//p[contains(text(), 'try again')]")).getText();
    }

    public void SignIn()
    {
        driver.findElement(By.xpath("//button[contains(text(), 'Sign In')]")).click();
    }
}

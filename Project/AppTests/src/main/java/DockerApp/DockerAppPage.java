package DockerApp;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

public class DockerAppPage
{
    private RemoteWebDriver driver;
    private String url = "http://172.23.0.5:3000";

    public DockerAppPage(RemoteWebDriver driver)
    {
        this.driver = driver;
        driver.navigate().to(url);
    }

    // simple method to wait on pages
    public void Stall(int seconds)
    {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    // closes driver
    public void Exit()
    {
        driver.quit();
    }

    @Step("Refreshing webpage")
    // refreshes webpage
    public void Refresh()
    {
        driver.navigate().refresh();
    }

    public RegisterPage AccountCreation()
    {
        return new RegisterPage(driver);
    }

    public SignInPage SignIn()
    {
        return new SignInPage(driver);
    }

    public DashboardPage Dashboard() {return new DashboardPage(driver); }

    @Step("Logging out")
    public String LogOut()
    {
        Stall(2);
        driver.findElement(By.xpath("/html/body/div/div/nav/div[2]/a")).click();
        Stall(3);

        return driver.findElement(By.xpath("//div[contains(text(),'Sign In')]")).getText();
    }

}

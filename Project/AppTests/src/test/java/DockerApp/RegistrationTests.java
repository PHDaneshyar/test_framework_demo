package DockerApp;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.*;
import Listeners.RegistrationTestsListener;

@Listeners({RegistrationTestsListener.class})
@Epic("Acceptance Tests")
@Feature("Registration Tests")
public class RegistrationTests extends BaseTest
{

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Story("User enters non-matching passwords when creating an account")
    @Description("Testing error messages on account creation")
    public void RegistrationPasswordError()
    {
        register.GoToRegisterPage();
        dockerApp.Stall(3);

        register.EnterDetails("koosha@gmail.com", "test", "testing", "Koo", "Sha" );
        register.Register();
        Assert.assertEquals("Passwords Do Not Match", register.PasswordErrorMessage());
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Story("User enters invalid email when creating an account")
    @Description("Testing error messages on account creation")
    public void RegistrationEmailError()
    {
        register.GoToRegisterPage();
        dockerApp.Stall(3);

        register.EnterDetails("koosha", "test", "test", "Koo", "Sha" );
        register.Register();
        Assert.assertEquals("Please enter a valid email address", register.EmailErrorMessage());
    }
}

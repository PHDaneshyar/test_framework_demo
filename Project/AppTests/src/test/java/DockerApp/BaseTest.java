package DockerApp;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.ne;

public class BaseTest
{
    public static RemoteWebDriver driver;

    static DockerAppPage dockerApp;
    static SignInPage signIn;
    static RegisterPage register;
    static DashboardPage dashboard;

    static int amount, accNum, txn;

    // these variables will be assigned values pulled from the mongodb
    static String firstName, lastName, email, password;

    static MongoClient client = MongoClients.create("mongodb://admin:password@172.23.0.8:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false");
    static MongoDatabase db;
    static MongoCollection<Document> UserCollection, DetailsCollection, TransactionCollection;

    static Document doc, doc2;

    @Parameters("Browser")
    @BeforeClass
    public static void Setup(String browser) throws MalformedURLException
    {
        Random rand = new Random();
        amount = rand.nextInt(1000);
        accNum = 80007000;

        // set the driver based on the browser parameter
        if (browser.equalsIgnoreCase("chrome"))
        {
            // stop save message popup from displaying
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver = new RemoteWebDriver(new URL("http://172.23.0.3:4444/wd/hub"), options);
        }
        else if(browser.equalsIgnoreCase("firefox"))
        {
            // firefox options creates a null pointer exception
            driver = new RemoteWebDriver(new URL("http://172.23.0.3:4444/wd/hub"), DesiredCapabilities.firefox());
        }


        db = client.getDatabase("docker-app");

        // connect to the user details table and pull the first collection without a null email
        DetailsCollection = db.getCollection("userdetails");

        doc = DetailsCollection.find(ne("email", null)).sort(Sorts.ascending("email")).first();

        email = doc.getString("email");

        // connect to the user table and pull the first connection that links to the email
        UserCollection = db.getCollection("users");

        doc2 = UserCollection.find(eq("email", email)).first();
        password = doc2.getString("pass");
        firstName = doc2.getString("firstname");
        lastName = doc2.getString("lastname");

        // set up the remote web driver
        dockerApp = new DockerAppPage(driver);
        dockerApp.Stall(2);

        // create page instances
        signIn = dockerApp.SignIn();
        register = dockerApp.AccountCreation();
        dashboard = dockerApp.Dashboard();

    }

    // for listeners to use the driver instance
    public static RemoteWebDriver getDriver()
    {
        return driver;
    }
}

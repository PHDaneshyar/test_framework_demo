package DockerApp;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

import Listeners.TransactionTestsListener;

@Listeners({TransactionTestsListener.class})
@Epic("Acceptance Tests")
@Feature("Transaction Tests")
public class TransactionTests extends BaseTest
{
    @BeforeClass
    public void TransactionSetup()
    {
        // Sign in to account
        signIn.SignInTab();
        signIn.Email(email);
        signIn.Password(password);
        signIn.SignIn();
    }


    @Test(priority = 1)
    @Severity(SeverityLevel.NORMAL)
    @Story("User tries to deposit money into account")
    @Description("Successful deposit test with correct details")
    public void Deposit() throws InterruptedException
    {
        dockerApp.Stall(2);
        dashboard.Deposit(amount, "Test Deposit", accNum, firstName, lastName);
        Thread.sleep(5000);
        Assert.assertEquals("£" + Integer.toString(amount) + ".00", "cheese");
    }

    @Test(priority = 1)
    @Severity(SeverityLevel.NORMAL)
    @Story("User tries to withdraw money from account")
    @Description("Successful withdrawal test with correct details")
    public void Withdraw() throws InterruptedException
    {
        dockerApp.Stall(3);
        dashboard.Withdraw(amount, "Test Withdraw", accNum, firstName, lastName);
        TimeUnit.SECONDS.sleep(5);
        Assert.assertEquals("-£" + Integer.toString(amount) + ".00", dashboard.TransactionCheck());

    }

    // closes browser to free up node
    @AfterClass
    public static void CleanUp()
    {
        dockerApp.Exit();
    }

}

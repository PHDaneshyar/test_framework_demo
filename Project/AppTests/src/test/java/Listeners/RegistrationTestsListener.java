package Listeners;

import DockerApp.RegistrationTests;
import DockerApp.TransactionTests;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class RegistrationTestsListener extends RegistrationTests implements ITestListener
{
    // screenshot attachment
    @Attachment(value = "Page Screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(RemoteWebDriver driver)
    {
        return ((TakesScreenshot)driver).getScreenshotAs((OutputType.BYTES));
    }

    // Test attachment
    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog(String message)
    {
        return message;
    }

    // get the name of the test that just ran
    private static String getTestName(ITestResult result)
    {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    // will capture screenshots and log when a test fails
    public void onTestFailure(ITestResult result)
    {
        System.out.println(getTestName(result) + " failed");

        // get the webdriver from the test class and assign it to local variable
        RemoteWebDriver driver = RegistrationTests.getDriver();

        // save a screenshot of failing test to allure
        if(driver instanceof RemoteWebDriver)
        {
            System.out.println("Screenshot captured for test after failure: " + getTestName(result));
            saveScreenshotPNG(driver);
        }

        // save a log on allure
        saveTextLog(getTestName(result) + " failed and a screenshot was taken");
    }

}

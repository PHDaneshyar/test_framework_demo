# Test Automation Framework

## Initial Setup

Before the Framework can be launched there a few simple requirements that need to be fulfilled.

1. Go to the .env file located within the project folder and change the COMPOSE_HOST_PATH variable to = the local filepath of the Framework i.e. /Users/Name/Documents/TestFramework
This is used in the docker-compose files for creating volumes.
2. Also within the .env file on a windows machine add the lines
`COMPOSE_CONVERT_WINDOWS_PATHS=1` `COMPOSE_FORCE_WINDOWS_HOST=1`
 On mac make sure these lines are removed or commented out.

## Usage

These commands needs to be ran from the folder containing the top level docker-compose file.

Always be sure to build the image first, this will make sure any changes are setup, and if there are any updates for plugins these will be automatically updated.

```
docker-compose build
```
Then run the below command to boot up the Framework.
```
docker-compose up
```

Navigate to localhost:8080 to access Jenkins. If it is a fresh build of the framework with no existing Jenkins data a new Jenkins set-up will be required, using an admin password that will be displayed in the console window.

## Components included in Framework

* Jenkins
* Gitbucket
* Selenium Grid
* Dummy app (for demo purposes)
* Load balancer (for dummy app)
* Mongodb (for  dummy app)
* Back-end (for dummy app)

## Swapping out Components

Components can be swapped out our added in to work with other frameworks. This is done by changing the appropriate docker-compose file and if the component being added/removed is a custom image then any related dockerfiles as well.

For example replacing gitbucket in the compose with the below example would change the container to be a gitlab container
```
gitlab:
  image: 'gitlab/gitlab-ce:latest'
  restart: always
  hostname: 'gitlab.example.com'
  environment:
    GITLAB_OMNIBUS_CONFIG: |
      external_url 'https://gitlab.example.com'
      # Add any other gitlab.rb configuration here, each on its own line
  ports:
    - '80:80'
    - '443:443'
    - '22:22'
  volumes:
    - '${COMPOSE_HOST_PATH}/gitlab/config:/etc/gitlab'
    - '${COMPOSE_HOST_PATH}/gitlab/logs:/var/log/gitlab'
    - '${COMPOSE_HOST_PATH}/gitlab/data:/var/opt/gitlab'
  networks:
    my-network:
      ipv4_address: 172.23.0.12
```

## Jenkins initial build

The custom Jenkins image in the framework comes with several plugins included for a more out-of-the-box start. These plugins include:

* allure
* git
* docker-compose
* docker
* blue-ocean
* jdk-11

These plugins are set to install the latest version, this allows for automatic updates of plugins every time the image is built.

#### Installing Tools and Plugins

Any other plugins or tools needed can be installed manually through Jenkins.

These can be added via the manage jenkins tab, there are two that need to be added for this initial setup to work, these are Maven and Allure.Although the plugin for allure is pre-installed we still need to add the Allure commandline tool.

Navigate to manage jenkins -> global tool configuration -> Allure commandline.
To add an installation simply click add allure commandline and give the installation a suitable name like "Allure".

Follow the same steps for Maven as well.

#### Add New Plugins to build

Each time a plugin is installed it is recommended to add the plugin to the plugins.txt file found at `jenkins_dockerfile/copyme`

To enable auto-updates when the dockerfile is built then add plugins in the format `workflow-support:latest` else replace latest with the version required to ensure manual updates are required.

## Containerised Repo

Within the framework is a containerised repository, this is to allow Jenkins to be able to poll and run builds after changes even if there is no internet connection, however it is also possible to use online repos that aren't containerised as well.

We have opted to use gitbucket for the demo framework, but any SCM tool could be used instead. This would only require changing the initial docker-compose as shown above.

It is recommended to only connect the parts of the directory related to job being run on Jenkins, i.e. The project folder, to the containerised repo. There is no need to push the Jenkins and Gitbucket folders to this repo as these are just local mounts for persistent data. However there is no reason why there couldn't be a repo containing the entire framework as a backup.

To setup a connection to the containerised repo navigate to localhost:9090 and then follow the standard steps to create a repo and link it with the current directory. If the directory is linked to another repo already then it is possible to set up a connection to multiple repos using the command

```
git remote add REMOTE-ID REMOTE-URL
```
More details on setting this up and how to work with multiple repos can be found here
[https://jigarius.com/blog/multiple-git-remote-repositories](https://jigarius.com/blog/multiple-git-remote-repositories)

#### Node Modules

In order to run the Mocha tests on the demo project the node modules need to be uploaded to the containerised repo, however by default these won't be tracked by git and will need to be forcibly added with the command
```
git add -f App/web-app/resources/node_modules
``` 

## Viewing Selenium Tests

In order to view tests being run with selenium grid in the browser window, a tool called VNC is needed as the tests will be run within containers. The selenium nodes already have VNC included so that they can be detected by the viewing tool.

Download and install the viewer tool from here:
[https://www.realvnc.com/en/connect/download/viewer/](https://www.realvnc.com/en/connect/download/viewer/) and then connect to the ports associated with the nodes.

## Volumes/Mounts

Mounting volumes is essential when working with docker containers wherever there is a need for persistent data. For example in our demo framework we needed the Mongodb container to retain user data and this was achieved by locally mounting the data in the container.

In the example below the mongo initialisation files and data files have been mounted to the local machine(left-hand side of the :), this allows the data to persist after composing down and spinning back up the container.

```
mongo:
  container_name: mongo
  image: mongo
  restart: on-failure
  volumes:
    - ${COMPOSE_HOST_PATH}/Project/App/mongo:/data/db/
    - ${COMPOSE_HOST_PATH}/Project/App/mongo/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro
```

## Docker in Docker

The framework utilises docker-in-docker, where after the initial containers are built Jenkins will then later create more containers during builds. This is done by exposing the docker sock as a volume in the docker-compose for Jenkins

```
volumes:
    - ${COMPOSE_HOST_PATH}/Jenkins:/var/jenkins_home
    - //var/run/docker.sock:/var/run/docker.sock
```

This means that all containers are running off of the host machines' docker daemon, for example run the command `docker ps` and all running containers including the ones built within the Jenkins container should be visible.
